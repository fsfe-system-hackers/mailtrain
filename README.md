# Mailtrain

The FSFE uses [Mailtrain](https://mailtrain.org/) to send out large amounts of mails.

This instance is connected with our [zoneMTA](https://git.fsfe.org/fsfe-system-hackers/zonemta) installation which is being send as a powerful MTA.
